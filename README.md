#json4bean
---
json生成JavaBean工具包

演示 DEMO
```
示例代码
public static void main(String[] args) {

    String data = "{\n" +
            "  \"id\": 123,\n" +
            "  \"name\": \"张三\",\n" +
            "  \"firend\": {\n" +
            "    \"fid\": \"f123\",\n" +
            "    \"fname\": \"李四\"\n" +
            "  },\n" +
            "  \"subjects\": [\n" +
            "    {\n" +
            "      \"sid\": \"o123\",\n" +
            "      \"sname\": \"王五\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"sid\": \"o124\",\n" +
            "      \"sname\": \"马六\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    String packageName = "com.javaear.test";
    JSON.writeBean(data, "Student");
    JSON.writeBean(data, "Student", packageName);
    JSON.writeBean(data, "Student", packageName, System.getProperty("user.dir") + File.separator + "src/test/java/" + "com/javaear/test");
}
生成效果
package com.javaear.test;

import java.util.List;

public class Student {
    private Long id;
    private String name;
    private Firend firend;
    private List<Subjects> subjects;

    public static class Firend {
        private String fid;
        private String fname;

        public String getFid() {
            return this.fid;
        }

        public void setFid(String fid) {
            this.fid = fid;
        }

        public String getFname() {
            return this.fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }
    }

    public static class Subjects {
        private String sid;
        private String sname;

        public String getSid() {
            return this.sid;
        }

        public void setSid(String sid) {
            this.sid = sid;
        }

        public String getSname() {
            return this.sname;
        }

        public void setSname(String sname) {
            this.sname = sname;
        }
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Firend getFirend() {
        return this.firend;
    }

    public void setFirend(Firend firend) {
        this.firend = firend;
    }

    public List<Subjects> getSubjects() {
        return this.subjects;
    }

    public void setSubjects(List<Subjects> subjects) {
        this.subjects = subjects;
    }
}
```

### 功能
1. 根据json字符串生成javaBean对象
2. 根据业务需要，后续会提供支持生成三种JavaBean选择
    - DTO 数据传输对象，多用于Api接口联调对接使用，对代码美化度、格式无硬性要求(目前已支持)
    - Model 数据模型，对Java开发者友好的字段排序方式(目前已支持)
    - DO 数据库持久层使用对象，驼峰式下划线转换(待定)
3. @JsonIgnore注解添加(后续支持，主要针对由于jsonkey为大写，造成生成大写字母开头的field)
4. 代码模板支持，比如getter setter自定义注释，类注释等(后续支持)
5. Json解析为对象模型，或对象转Json(后续支持，有待商榷)
6. JsonArray生成JavaBean对象(后续支持，目前仅支持JsonObject类字符串生成JavaBean对象)
7. Json字符串json value为Null，生成的属性类型后续改为Object(因为根据Value Null暂时无法知道具体属性类型)
8. Json字符串内嵌的对象，生成多个独立的JavaBean(后续支持，目前仅支持生成内部类形式)

### 人群
1. 经常进行api接口联调对接的java开发者，由于接口提供者未提供sdk包，对接人员经常需要根据request、response的json示例去写javaBean，这种大量重复且枯燥的工作，完全可以用json4bean轻松搞定。 ^ _ ^

2.其他需要根据json生成javaBean的不明真相的吃瓜群众。 ^ _ ^

