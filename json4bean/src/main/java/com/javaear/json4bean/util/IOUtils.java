package com.javaear.json4bean.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author aooer
 */
public abstract class IOUtils {

    /**
     * 写字符串到文件
     *
     * @param sourceStr 原字符串
     * @param destFile  目标
     * @throws IOException
     */
    public static void write(String sourceStr, String destFile) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File(destFile)));
        writer.write(sourceStr);
        writer.flush();
        writer.close();
    }
}
