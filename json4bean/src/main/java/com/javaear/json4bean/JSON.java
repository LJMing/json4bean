package com.javaear.json4bean;

import com.javaear.json4bean.core.JsonRender;
import com.javaear.json4bean.exception.Json4BeanWriterIOException;
import com.javaear.json4bean.util.StringUtils;

import java.io.File;
import java.io.IOException;

/**
 * @author aooer
 */
public abstract class JSON {

    /**
     * 默认生成文件目录
     */
    private static final String DEFAULT_DEST_DIR = System.getProperty("user.dir");

    /**
     * 根据json字符串
     * 写javaBean文件
     *
     * @param jsonStr   json字符串
     * @param className 类名
     */
    public static void writeBean(String jsonStr, String className) {
        writeBean(jsonStr, className, null);
    }

    /**
     * 根据json字符串
     * 写javaBean文件
     *
     * @param jsonStr   json字符串
     * @param className 类名
     */
    public static void writeBean(String jsonStr, String className, String packageName) {
        writeBean(jsonStr, className, packageName, DEFAULT_DEST_DIR);
    }

    /**
     * 根据json字符串
     * 写javaBean文件
     *
     * @param jsonStr   json字符串
     * @param className 类名
     * @param destDir   目标文件路径
     */
    public static void writeBean(String jsonStr, String className, String packageName, String destDir) {
        try {
            if ((StringUtils.isEmpty(destDir) || destDir.equals(DEFAULT_DEST_DIR)) && !StringUtils.isEmpty(packageName))
                destDir += File.separator + packageName.replace(".", File.separator);
            if (!new File(destDir).exists()) new File(destDir).mkdirs();
            JsonRender.getInstance().reander(jsonStr, className, packageName, destDir);
        } catch (IOException e) {
            throw new Json4BeanWriterIOException(e);
        }
    }

}
