package com.javaear.json4bean.core;

import com.javaear.json4bean.util.StringUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * @author aooer
 */
abstract class JsonSerializer implements Serializable {

    /**
     * 校验json
     *
     * @param json json
     */
    private static void validate(Object json) {
        if (!(json instanceof LinkedHashMap))
            throw new IllegalArgumentException("jsonstr validate exception");
    }

    /**
     * 序列化json对象
     *
     * @param jsonObj     jsonObj
     * @param className   className
     * @param prefixSpace 前置空间
     * @return bean content内容
     */
    static String serialize(Object jsonObj, String className, final String prefixSpace) {
        validate(jsonObj);
        StringBuilder stringBuilder = new StringBuilder(StringUtils.LINE);
        StringBuilder methodBuilder = new StringBuilder();
        StringBuilder innerBuilder = new StringBuilder();
        stringBuilder.append(prefixSpace.substring(4)).append("public ").append(prefixSpace.equals(StringUtils.PREFIX_SPACE) ? "" : "static ").append("class ").append(className).append(" {");
        ((LinkedHashMap<?, ?>) jsonObj).entrySet().stream().forEach(e -> {
            String key = StringUtils.upperCaseFirstChar((String) e.getKey());
            Object value = e.getValue();
            //list集合
            if (value instanceof ArrayList) {
                Object fieldFirst = ((ArrayList) value).get(0);
                String fieldType = fieldFirst.getClass().getSimpleName();
                if (fieldFirst instanceof ArrayList)
                    fieldType = "List";
                if (fieldFirst instanceof LinkedHashMap)
                    fieldType = key;
                stringBuilder.append(newLine(prefixSpace)).append("private ").append("List<").append(fieldType).append(">").append(" ").append(e.getKey()).append(";");
                appendGetterSetter(methodBuilder, prefixSpace, "List<" + fieldType + ">", (String) e.getKey());
                if (fieldFirst instanceof ArrayList || fieldFirst instanceof LinkedHashMap)
                    innerBuilder.append(StringUtils.LINE).append(serialize(fieldFirst, key, prefixSpace + prefixSpace));
            } else if (value instanceof LinkedHashMap) {
                stringBuilder.append(newLine(prefixSpace)).append("private ").append(key).append(" ").append(e.getKey()).append(";");
                appendGetterSetter(methodBuilder, prefixSpace, key, (String) e.getKey());
                innerBuilder.append(StringUtils.LINE).append(serialize(value, key, prefixSpace + prefixSpace));
            } else {
                stringBuilder.append(newLine(prefixSpace)).append("private ").append(value.getClass().getSimpleName()).append(" ").append(e.getKey()).append(";");
                appendGetterSetter(methodBuilder, prefixSpace, value.getClass().getSimpleName(), (String) e.getKey());
            }
        });
        stringBuilder.append(innerBuilder).append(methodBuilder).append(newLine(prefixSpace.substring(4))).append("}");
        return stringBuilder.toString();
    }

    /**
     * 追加getter setter
     *
     * @param sourceBuilder sourceStr 源字符串
     * @param prefixSpace   前置空间
     * @param fieldType     字段类型
     * @param fieldName     字段名称
     */
    private static void appendGetterSetter(StringBuilder sourceBuilder, String prefixSpace, String fieldType, String fieldName) {
        sourceBuilder
                .append(StringUtils.LINE)
                .append(newLine(prefixSpace))
                .append("public ").append(fieldType).append(" get").append(StringUtils.upperCaseFirstChar(fieldName)).append("() {").append(newLine(prefixSpace + StringUtils.PREFIX_SPACE)).append("return this.").append(fieldName).append(";").append(newLine(prefixSpace)).append("}")
                .append(StringUtils.LINE)
                .append(newLine(prefixSpace))
                .append("public void set").append(StringUtils.upperCaseFirstChar(fieldName)).append("(").append(fieldType).append(" ").append(fieldName).append(") {").append(newLine(prefixSpace + StringUtils.PREFIX_SPACE)).append("this.").append(fieldName).append(" = ").append(fieldName).append(";").append(newLine(prefixSpace)).append("}");
    }

    /**
     * 新行 前置空间
     *
     * @param prefixSpace prefixSpace
     * @return newLineStr
     */
    private static String newLine(String prefixSpace) {
        return StringUtils.LINE + prefixSpace;
    }

}
