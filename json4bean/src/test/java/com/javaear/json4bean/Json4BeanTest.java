package com.javaear.json4bean;

import java.io.File;

public class Json4BeanTest {

    public static void main(String[] args) {

        String data = "{\n" +
                "  \"id\": 123,\n" +
                "  \"name\": \"张三\",\n" +
                "  \"firend\": {\n" +
                "    \"fid\": \"f123\",\n" +
                "    \"fname\": \"李四\"\n" +
                "  },\n" +
                "  \"subjects\": [\n" +
                "    {\n" +
                "      \"sid\": \"o123\",\n" +
                "      \"sname\": \"王五\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"sid\": \"o124\",\n" +
                "      \"sname\": \"马六\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        String packageName = "com.javaear.test";
        JSON.writeBean(data, "Student");
        JSON.writeBean(data, "Student", packageName);
        JSON.writeBean(data, "Student", packageName, System.getProperty("user.dir") + File.separator + "src/test/java/" + "com/javaear/test");
    }

}
